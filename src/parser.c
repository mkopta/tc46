#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "parser.h"

#define ID_SIZE 2048

char id[ID_SIZE];
char msg[] = "Wild chipmunk ate your program.\n";
int count;

enum token_class {
	TOKEN_LP, /* ( */
	TOKEN_RP, /* ) */
	TOKEN_ID, /* [0-9a-Z]+[.]?[0-9a-Z]* */
	TOKEN_COMMA, /* , */
	TOKEN_EOF,
	TOKEN_ERROR
};

int token(FILE *fp);
struct expression **parse_fcalls(FILE *fp);
struct expression *parse(FILE *fp);
struct expression *parse_fcall(FILE *fp);
void parse_lp(FILE *fp, enum token_class t);
void token_undo(FILE *fp, enum token_class t);

struct expression *parse(FILE *fp)
{
	enum token_class t;
	t = token(fp);
	if (t == TOKEN_ID) {
		return parse_fcall(fp);
	} else {
		return NULL;
	}
}

struct expression *parse_fcall(FILE *fp)
{
	enum token_class t;
	struct expression *ex;
	ex = malloc(sizeof(struct expression));
	if (!ex) {
		fputs("Memory error\n", stderr);
		exit(1);
	}
	ex->id = strdup(id);
	t = token(fp);
	if (t == TOKEN_RP || t == TOKEN_COMMA || t == TOKEN_EOF) {
		token_undo(fp, t);
		ex->subex = NULL;
		ex->subex_count = 0;
	} else {
		parse_lp(fp, t);
		ex->subex = parse_fcalls(fp);
		ex->subex_count = count;
	}
	return ex;
}

struct expression **parse_fcalls(FILE *fp)
{
	enum token_class t;
	struct expression *ex;
	int expecting_comma = 0;
	int fcalls_count = 0;
	struct expression **exs = malloc(
		fcalls_count * sizeof(struct expression *));
PARSE_FCALLS_LOOP_START:
	t = token(fp);
	if (t == TOKEN_ERROR) {
		exit(1);
	} else if (t == TOKEN_RP && (expecting_comma || fcalls_count == 0)) {
		goto PARSE_FCALLS_LOOP_END;
	} else if (t == TOKEN_ID) {
		ex = parse_fcall(fp);
		expecting_comma = 1;
	} else if (t == TOKEN_COMMA && expecting_comma) {
		expecting_comma = 0;
		goto PARSE_FCALLS_LOOP_START;
	} else {
		fputs(msg, stderr);
		exit(1);
	}
	exs = realloc(exs,
		++fcalls_count * sizeof(struct expression *));
	if (!exs) {
		fputs("Memory error\n", stderr);
		exit(1);
	}
	*(exs + fcalls_count - 1) = ex;
	goto PARSE_FCALLS_LOOP_START;
PARSE_FCALLS_LOOP_END:
	count = fcalls_count;
	return exs;
}

void parse_lp(FILE *fp, enum token_class t)
{
	if (t != TOKEN_LP) {
		fputs(msg, stderr);
		exit(1);
	}
}

int token(FILE *fp)
{
	int c, state = 0, id_index = 0;
	if (!fp)
		return TOKEN_ERROR;
LOOP:
#define isid(c) (isalnum(c) || (c) == '.' || (c) == '-' || (c) == '_')
	c = fgetc(fp);
	switch(state) {
	case 0:
		if (isspace(c))
			break;
		if (c == EOF)
			return TOKEN_EOF;
		if (c == '(')
			return TOKEN_LP;
		if (c == ')')
			return TOKEN_RP;
		if (c == ',')
			return TOKEN_COMMA;
		if (isid(c)) {
			memset(id, '\0', sizeof(char) * ID_SIZE);
			id[id_index++] = c;
			state = 1;
			break;
		}
		fputs("Unknown character! Duck and cover!\n", stderr);
		return TOKEN_ERROR;
	case 1:
		if (isid(c)) {
			id[id_index++] = c;
			if (id_index == ID_SIZE) {
				fputs("Identifier TL;DR.\n", stderr);
				return TOKEN_ERROR;
			}
		} else {
			ungetc(c, fp);
			return TOKEN_ID;
		}
		break;
	default:
		fputs("State is a lie.\n", stderr);
		return TOKEN_ERROR;
	}
	goto LOOP;
}

void token_undo(FILE *fp, enum token_class t)
{
	switch(t) {
	case TOKEN_EOF:
		/* TODO ungetc(EOF, fp); */
		break;
	case TOKEN_LP:
		ungetc('(', fp);
		break;
	case TOKEN_RP:
		ungetc(')', fp);
		break;
	case TOKEN_COMMA:
		ungetc(',', fp);
		break;
	default:
		fputs("Undo? Oh dear!\n", stderr);
		exit(1);
	}
}

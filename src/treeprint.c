#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "treeprint.h"

enum prefix_type {
	PF_NONE = 0,
	PF_MIDDLE = 1,
	PF_LAST = 2
};

static const char *prefix_str[][3] = {
	{ "", "|-", "`-" },
	{ "", "| ", "  " }
};

#define PREFIX_SIZE 80
#define PREFIX_ELEM_SIZE 2

static char prefix[PREFIX_SIZE + 1];

static void _treeprint(struct expression *ex, enum prefix_type pt)
{
	int i = 0;
	char *end = NULL;
	if (!ex)
		return;
	end = prefix;
	while (*end)
		end++;
	if (end - prefix > PREFIX_SIZE - PREFIX_ELEM_SIZE) {
		fputs("tree is too tall\n", stderr);
		exit(1);
	}
	printf("%s%s %s\n", prefix, prefix_str[0][pt], ex->id);
	for (i = 0; i < ex->subex_count; i++) {
		strcpy(end, prefix_str[1][pt]);
		if (i + 1 < ex->subex_count) {
			_treeprint(ex->subex[i], PF_MIDDLE);
		} else {
			_treeprint(ex->subex[i], PF_LAST);
		}
	}
	end[0] = '\0';
}

void treeprint(struct expression *ex)
{
	prefix[0] = '\0';
	_treeprint(ex, PF_NONE);
}


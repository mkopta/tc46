#include <stdlib.h>
#include <string.h>
#include "dotprint.h"

static void head_print(FILE *fp)
{
	fprintf (fp, "%s", "digraph AST {\n");
}

static void tail_print(FILE *fp)
{
	fprintf (fp, "%s", "}\n");
}

static void node_print(const char *name, struct expression *ex, FILE *fp)
{
	int i;
	char *new_name = NULL, *new_part = NULL;
	fprintf(fp, "\"%s\"[label=\"%s\"] ;\n", name, ex->id);
	new_name = malloc(sizeof(char) * (strlen(name) + 15));
	if (!new_name) {
		fputs ("Memory error.\n", stderr);
		exit(1);
	}
	strcpy(new_name, name);
	new_part = new_name + strlen(new_name);
	for (i = 0; i < ex->subex_count; i++) {
		/* FIXME: check if snprintf didn't cut string */
		snprintf(new_part, 15, ".%x", i);
		node_print(new_name, ex -> subex[i], fp);
		fprintf(fp, "\"%s\" -> \"%s\" ;\n", name, new_name);
	}
	free(new_name);
}

void dotprint(struct expression *ex, FILE *fp)
{
	head_print(fp);
	node_print("0", ex, fp);
	tail_print(fp);
}

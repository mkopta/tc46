#ifndef _parser_h_
#define _parser_h_

#include <stdio.h>
#include "expr.h"

struct expression *parse(FILE *fp);

void expr_free (struct expression *ex);

#endif

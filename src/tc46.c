#include <string.h>
#include <stdlib.h>
#include "expr.h"
#include "parser.h"
#include "dotprint.h"

int main(void)
{
	struct expression *ex;
	ex = parse(stdin);
	return interp(ex);
}

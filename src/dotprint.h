#ifndef _dotprint_h_
#define _dotprint_h_

#include <stdio.h>
#include "expr.h"

void dotprint(struct expression *ex, FILE *fp);

#endif /* _dotprint_h_ */

#ifndef _interp_h_
#define _interp_h_

#include "expr.h"

int interp(struct expression *ex);

#endif

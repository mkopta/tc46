#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "interp.h"

enum fclass
{
	FC_BUILTIN,
	FC_NUMBER,
	FC_STRING,
	FC_USER,
	FC_UNKNOWN
};

enum builtin
{
	BI_EXIT,
	BI_SEQ
};

struct function
{
	enum fclass fclass;
	union
	{
		enum builtin builtin;
		double number;
		char *string;
	} d;
};

struct function funfind(char *name);
int is_number(char *name, double *dout);

int interp(struct expression *ex)
{
	struct function f;
	if (ex == NULL)
		return 1;
	f = funfind(ex->id);
	switch(f.fclass) {
	case FC_USER:
		fputs("Yeah, sure. Whatever.\n", stderr);
		break;
	case FC_BUILTIN:
		if (f.d.builtin == BI_EXIT)
			exit(0);
		break;
	case FC_NUMBER:
		printf("%f\n", f.d.number);
		break;
	case FC_UNKNOWN:
		fprintf(stderr, "Function '%s' is undefined.\n", ex->id);
		return 1;
		break;
	default:
		fputs("I don't usually crash, but when I do, "
			"I don't say why.\n", stderr);
		return 1;
	}
	return 0;
}

struct function funfind(char *name)
{
	struct function f;
	double d = 0.0;
	f.fclass = FC_UNKNOWN;
	if (strcmp (name, "exit") == 0)
	{
		f.fclass = FC_BUILTIN;
		f.d.builtin = BI_EXIT;
		return f;
	} else if (is_number(name, &d)) {
		f.fclass = FC_NUMBER;
		f.d.number = d;
		return f;
	} else {
		f.fclass = FC_UNKNOWN;
	}
	return f;
}

int is_number(char *fname, double *dout)
{
	double d = 0.0;
	char *end = NULL;
	d = strtod(fname, &end);
	if (end == NULL
		|| end == fname
		|| *end != '\0'
		|| d != d /* ask IEEE */
		|| d == HUGE_VAL
		|| d == -HUGE_VAL)
		return 0;
	*dout = d;
	return 1;
}

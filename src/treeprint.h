#ifndef _treeprint_h_
#define _treeprint_h_

#include "expr.h"

void treeprint(struct expression *ex);

#endif /* _treeprint_h_ */

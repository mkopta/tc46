#ifndef _expr_h_
#define _expr_h_

struct expression {
	char *id;
	int subex_count;
	struct expression **subex;
};

#endif
